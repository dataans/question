FROM rust:latest

RUN USER=root cargo new --bin question

WORKDIR ./question

COPY ./Cargo.toml ./Cargo.toml

RUN cargo build --release
