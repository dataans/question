use std::{convert::TryFrom, sync::Arc};

use async_mutex::Mutex;
use deadpool_postgres::{Object, Pool, PoolError};
use uuid::Uuid;

use super::DbError;
use crate::model::Tag;

const ADD: &str =
    "insert into tags (pk, id, created_at, name, creator_id) values ($1, $2, $3, $4, $5)";
const FIND_BY_ID: &str = "select * from tags where id = $1";
const ADD_TAG_TO_QUESTION: &str =
    "insert into questions2tags (question_id, tag_id) values ($1, $2)";
const ADD_TAGS_TO_QUESTION: &str =
    "insert into questions2tags (question_id, tag_id) select $1, ids from unnest($2::uuid[]) as ids";
const DELETE_QUESTION_TAGS: &str = "delete from questions2tags where question_id = $1";
const GET_ALL_TAGS: &str = "select * from tags";
const GET_QUESTION_TAGS: &str = "select tags.pk, tags.id, tags.name, tags.created_at, tags.creator_id from tags left join questions2tags on questions2tags.tag_id = tags.pk where questions2tags.question_id = $1";
const COUNT_TAG_QUESTIONS: &str = "select count(tags.id) from tags inner join questions2tags as q2t on q2t.tag_id = tags.pk where q2t.tag_id = $1";

pub struct TagRepository {
    pool: Arc<Mutex<Pool>>,
}

impl TagRepository {
    pub fn new(pool: Arc<Mutex<Pool>>) -> Self {
        Self { pool }
    }

    async fn get_connection(&self) -> Result<Object, PoolError> {
        self.pool.lock().await.get().await
    }

    pub async fn save(&self, data: &Tag) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(ADD).await?;

        client
            .execute(
                &smt,
                &[
                    &data.pk,
                    &data.id,
                    &data.created_at,
                    &data.name,
                    &data.creator_id,
                ],
            )
            .await?;

        Ok(())
    }

    pub async fn find_by_id(&self, id: &str) -> Result<Option<Tag>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_BY_ID).await?;

        match client.query(&smt, &[&id]).await?.into_iter().next() {
            Some(row) => Ok(Some(Tag::try_from(row)?)),
            None => Ok(None),
        }
    }

    pub async fn get_all_tags(&self) -> Result<Vec<Tag>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(GET_ALL_TAGS).await?;

        let mut tags = Vec::new();
        for row in client.query(&smt, &[]).await? {
            tags.push(Tag::try_from(row)?);
        }

        Ok(tags)
    }

    pub async fn get_question_tags(&self, question_id: &Uuid) -> Result<Vec<Tag>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(GET_QUESTION_TAGS).await?;

        let mut tags = Vec::new();
        for row in client.query(&smt, &[&question_id]).await? {
            tags.push(Tag::try_from(row)?);
        }

        Ok(tags)
    }

    pub async fn count_tag_questions(&self, tag_id: &Uuid) -> Result<i64, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(COUNT_TAG_QUESTIONS).await?;

        match client.query(&smt, &[&tag_id]).await?.into_iter().next() {
            Some(row) => Ok(row.try_get::<_, i64>(0)?),
            None => Ok(0),
        }
    }

    pub async fn add_tag_to_question(
        &self,
        question_id: &Uuid,
        tag_id: &Uuid,
    ) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(ADD_TAG_TO_QUESTION).await?;

        client.execute(&smt, &[&question_id, &tag_id]).await?;

        Ok(())
    }

    pub async fn add_tags(&self, question_id: &Uuid, tags: &Vec<Uuid>) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(ADD_TAGS_TO_QUESTION).await?;

        client.execute(&smt, &[question_id, tags]).await?;

        Ok(())
    }

    pub async fn remove_tags(&self, question_id: &Uuid) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(DELETE_QUESTION_TAGS).await?;

        client.execute(&smt, &[question_id]).await?;

        Ok(())
    }
}
