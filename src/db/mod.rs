pub mod answer;
pub mod question;
pub mod tag;

use deadpool_postgres::PoolError;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum DbError {
    #[error("Database unavailable")]
    Unavailable,
    #[error("Query error: {0:?}")]
    QueryError(#[from] deadpool_postgres::tokio_postgres::Error),
    #[error("Db connection error: {0:?}")]
    DbConnectionError(#[from] PoolError),
}
