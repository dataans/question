use std::{convert::TryFrom, sync::Arc};

use async_mutex::Mutex;
use deadpool_postgres::{Object, Pool, PoolError};
use uuid::Uuid;

use super::DbError;
use crate::model::{Answer, AnswerVote};

const ADD_ANSWER: &str = "insert into answers (id, text, created_at, updated_at, creator_id, sample_id) values ($1, $2, $3, $4, $5, $6)";
const FIND_BY_ID: &str =
    "select id, text, created_at, updated_at, creator_id, sample_id from answers where id = $1";
const UPDATE_ANSWER: &str =
    "update answers set text = $2, sample_id = $3, updated_at = $4 where id = $1";

const SELECT_QUESTION_ANSWERS: &str = "select a.id, a.text, a.created_at, a.updated_at, a.creator_id, a.sample_id from answers as a inner join questions_answers as qa on qa.answer_id = a.id where qa.question_id = $1";

const QUERY_CREATED_ANSWERS: &str = "select id, text, created_at, updated_at, creator_id, sample_id from answers where creator_id = $1";
const QUERY_VOTED_ANSWERS: &str = "select a.id, a.text, a.created_at, a.updated_at, a.creator_id, a.sample_id from answers as a inner join answers_votes as av on av.answer_id = a.id where av.user_id = $1";

const VOTE: &str = "insert into answers_votes (answer_id, user_id, value) values ($1, $2, $3);";
const DELETE_VOTE: &str = "delete from answers_votes where answer_id = $1 and user_id = $2";
const FIND_VOTE: &str =
    "select answer_id, user_id, value from answers_votes where answer_id = $1 and user_id = $2";
const COUNT_VOTES_BY_ID: &str =
    "select coalesce(sum(value), 0) from answers_votes where answer_id = $1";
const UPDATE_VOTE: &str =
    "update answers_votes set value = $3 where answer_id = $1 and user_id = $2";

pub struct AnswerRepository {
    pool: Arc<Mutex<Pool>>,
}

impl AnswerRepository {
    pub fn new(pool: Arc<Mutex<Pool>>) -> Self {
        Self { pool }
    }

    async fn get_connection(&self) -> Result<Object, PoolError> {
        self.pool.lock().await.get().await
    }

    pub async fn update_vote(&self, vote: &AnswerVote) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(UPDATE_VOTE).await?;

        let vote_type: i16 = (&vote.vote_type).into();

        client
            .query(&smt, &[&vote.answer_id, &vote.user_id, &vote_type])
            .await?;

        Ok(())
    }

    pub async fn count_votes(&self, answer_id: &Uuid) -> Result<i64, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(COUNT_VOTES_BY_ID).await?;

        match client.query(&smt, &[&answer_id]).await?.into_iter().next() {
            Some(row) => Ok(row.try_get::<_, i64>(0)?),
            None => Ok(0),
        }
    }

    pub async fn find_vote(
        &self,
        answer_id: &Uuid,
        user_id: &Uuid,
    ) -> Result<Option<AnswerVote>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_VOTE).await?;

        match client
            .query(&smt, &[answer_id, user_id])
            .await?
            .into_iter()
            .next()
        {
            Some(row) => Ok(Some(AnswerVote::try_from(row)?)),
            None => Ok(None),
        }
    }

    pub async fn delete_vote(&self, answer_id: &Uuid, user_id: &Uuid) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(DELETE_VOTE).await?;

        client.execute(&smt, &[answer_id, user_id]).await?;

        Ok(())
    }

    pub async fn vote(&self, vote: &AnswerVote) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(VOTE).await?;

        let vote_type: i16 = (&vote.vote_type).into();

        client
            .query(&smt, &[&vote.answer_id, &vote.user_id, &vote_type])
            .await?;

        Ok(())
    }

    pub async fn save(&self, data: &Answer) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(ADD_ANSWER).await?;

        client
            .query(
                &smt,
                &[
                    &data.id,
                    &data.text,
                    &data.created_at,
                    &data.updated_at,
                    &data.creator_id,
                    &data.sample_id,
                ],
            )
            .await?;

        Ok(())
    }

    pub async fn find_by_id(&self, id: &Uuid) -> Result<Option<Answer>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_BY_ID).await?;

        match client.query(&smt, &[&id]).await?.into_iter().next() {
            Some(row) => Ok(Some(Answer::try_from(row)?)),
            None => Ok(None),
        }
    }

    async fn query_answers_by_uuid(&self, query: &str, id: &Uuid) -> Result<Vec<Answer>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(query).await?;

        let rows = client.query(&smt, &[&id]).await?;

        let mut answers = Vec::with_capacity(rows.len());
        for row in rows {
            answers.push(Answer::try_from(row)?);
        }

        Ok(answers)
    }

    pub async fn get_question_answers(&self, question_id: &Uuid) -> Result<Vec<Answer>, DbError> {
        self.query_answers_by_uuid(SELECT_QUESTION_ANSWERS, question_id)
            .await
    }

    pub async fn query_voted_answers(&self, user_id: &Uuid) -> Result<Vec<Answer>, DbError> {
        self.query_answers_by_uuid(QUERY_VOTED_ANSWERS, user_id)
            .await
    }

    pub async fn query_created_answers(&self, user_id: &Uuid) -> Result<Vec<Answer>, DbError> {
        self.query_answers_by_uuid(QUERY_CREATED_ANSWERS, user_id)
            .await
    }

    pub async fn update(&self, answer: &Answer) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(UPDATE_ANSWER).await?;

        client
            .execute(
                &smt,
                &[
                    &answer.id,
                    &answer.text,
                    &answer.sample_id,
                    &answer.updated_at,
                ],
            )
            .await?;

        Ok(())
    }
}
