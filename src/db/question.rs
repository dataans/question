use std::{convert::TryFrom, sync::Arc};

use async_mutex::Mutex;
use deadpool_postgres::{Object, Pool, PoolError};
use uuid::Uuid;

use super::DbError;
use crate::model::{Question, SavedQuestion, Vote};

const ADD_QUESTION: &str = "insert into questions (pk, id, title, text, created_at, updated_at, creator_id, sample_id) values ($1, $2, $3, $4, $5, $6, $7, $8)";
const FIND_BY_ID: &str = "select * from questions where id = $1";
const ADD_QUESTION_ANSWER: &str =
    "insert into questions_answers (question_id, answer_id) values ($1, $2)";
const UPDATE_QUESTION: &str =
    "update questions set title = $2, text = $3, updated_at = $4, sample_id = $5 where id = $1";

const QUERY_QUESTIONS_BY_TAG: &str = "select pk, id, title, text, created_at, updated_at, creator_id, views, sample_id from questions as q inner join questions2tags as q2t on q2t.question_id = q.pk where q2t.tag_id = $1";
const QUERY_SAVED_QUESTIONS: &str = "select pk, id, title, text, created_at, updated_at, creator_id, views, sample_id from questions as q inner join saved_questions as sq on sq.question_id = q.pk where sq.user_id = $1";
const QUERY_CREATED_QUESTIONS: &str = "select pk, id, title, text, created_at, updated_at, creator_id, views, sample_id from questions where creator_id = $1";
const QUERY_VOTED_QUESTIONS: &str = "select pk, id, title, text, created_at, updated_at, creator_id, views, sample_id from questions as q inner join questions_upvotes as qv on qv.question_id = q.pk where qv.user_id = $1";

const VOTE: &str = "insert into questions_upvotes (question_id, user_id) values ($1, $2);";
const DELETE_VOTE: &str = "delete from questions_upvotes where question_id = $1 and user_id = $2";
const FIND_VOTE: &str = "select * from questions_upvotes where question_id = $1 and user_id = $2";
const COUNT_VOTES_BY_ID: &str =
    "select count(user_id) from questions_upvotes where question_id = $1";

const ADD_TO_SAVED: &str = "insert into saved_questions (question_id, user_id) values ($1, $2);";
const DELETE_FROM_SAVED: &str =
    "delete from saved_questions where question_id = $1 and user_id = $2";
const FIND_IN_SAVED: &str = "select * from saved_questions where question_id = $1 and user_id = $2";
const COUNT_SAVED_BY_ID: &str = "select count(user_id) from saved_questions where question_id = $1";

pub struct QuestionRepository {
    pool: Arc<Mutex<Pool>>,
}

impl QuestionRepository {
    pub fn new(pool: Arc<Mutex<Pool>>) -> Self {
        Self { pool }
    }

    async fn get_connection(&self) -> Result<Object, PoolError> {
        self.pool.lock().await.get().await
    }

    pub async fn save(&self, data: &Question) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(ADD_QUESTION).await?;

        client
            .query(
                &smt,
                &[
                    &data.pk,
                    &data.id,
                    &data.title,
                    &data.text,
                    &data.created_at,
                    &data.updated_at,
                    &data.creator_id,
                    &data.sample_id,
                ],
            )
            .await?;

        Ok(())
    }

    pub async fn find_by_id(&self, id: &str) -> Result<Option<Question>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_BY_ID).await?;

        match client.query(&smt, &[&id]).await?.into_iter().next() {
            Some(row) => Ok(Some(Question::try_from(row)?)),
            None => Ok(None),
        }
    }

    pub async fn find_in_saved(
        &self,
        user_id: &Uuid,
        question_id: &Uuid,
    ) -> Result<Option<SavedQuestion>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_IN_SAVED).await?;

        match client
            .query(&smt, &[&question_id, &user_id])
            .await?
            .into_iter()
            .next()
        {
            Some(row) => Ok(Some(SavedQuestion::try_from(row)?)),
            None => Ok(None),
        }
    }

    pub async fn find_vote(
        &self,
        user_id: &Uuid,
        question_id: &Uuid,
    ) -> Result<Option<Vote>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_VOTE).await?;

        match client
            .query(&smt, &[&question_id, &user_id])
            .await?
            .into_iter()
            .next()
        {
            Some(row) => Ok(Some(Vote::try_from(row)?)),
            None => Ok(None),
        }
    }

    pub async fn add_vote(&self, user_id: &Uuid, question_id: &Uuid) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(VOTE).await?;

        client.execute(&smt, &[&question_id, &user_id]).await?;

        Ok(())
    }

    pub async fn update(&self, question: &Question) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(UPDATE_QUESTION).await?;

        client
            .execute(
                &smt,
                &[
                    &question.id,
                    &question.title,
                    &question.text,
                    &question.updated_at,
                    &question.sample_id,
                ],
            )
            .await?;

        Ok(())
    }

    pub async fn add_to_saved(&self, user_id: &Uuid, question_id: &Uuid) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(ADD_TO_SAVED).await?;

        client.execute(&smt, &[&question_id, &user_id]).await?;

        Ok(())
    }

    pub async fn delete_vote(&self, user_id: &Uuid, question_id: &Uuid) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(DELETE_VOTE).await?;

        client.execute(&smt, &[&question_id, &user_id]).await?;

        Ok(())
    }

    pub async fn delete_from_saved(
        &self,
        user_id: &Uuid,
        question_id: &Uuid,
    ) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(DELETE_FROM_SAVED).await?;

        client.execute(&smt, &[&question_id, &user_id]).await?;

        Ok(())
    }

    pub async fn count_votes_by_id(&self, question_id: &Uuid) -> Result<i64, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(COUNT_VOTES_BY_ID).await?;

        match client
            .query(&smt, &[&question_id])
            .await?
            .into_iter()
            .next()
        {
            Some(row) => Ok(row.try_get::<_, i64>(0)?),
            None => Ok(0),
        }
    }

    pub async fn count_saved_questions(&self, question_id: &Uuid) -> Result<i64, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(COUNT_SAVED_BY_ID).await?;

        match client
            .query(&smt, &[&question_id])
            .await?
            .into_iter()
            .next()
        {
            Some(row) => Ok(row.try_get::<_, i64>(0)?),
            None => Ok(0),
        }
    }

    pub async fn add_question_answer(
        &self,
        question_id: &Uuid,
        answer_id: &Uuid,
    ) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(ADD_QUESTION_ANSWER).await?;

        client.query(&smt, &[&question_id, &answer_id]).await?;

        Ok(())
    }

    async fn query_questions_by_uuid(
        &self,
        query: &str,
        id: &Uuid,
    ) -> Result<Vec<Question>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(query).await?;

        let rows = client.query(&smt, &[id]).await?;

        let mut questions = Vec::with_capacity(rows.len());

        for row in rows {
            questions.push(Question::try_from(row)?)
        }

        Ok(questions)
    }

    pub async fn query_questions_by_tag_id(&self, tag_id: &Uuid) -> Result<Vec<Question>, DbError> {
        self.query_questions_by_uuid(QUERY_QUESTIONS_BY_TAG, tag_id)
            .await
    }

    pub async fn query_saved_questions(&self, user_id: &Uuid) -> Result<Vec<Question>, DbError> {
        self.query_questions_by_uuid(QUERY_SAVED_QUESTIONS, user_id)
            .await
    }

    pub async fn query_voted_questions(&self, user_id: &Uuid) -> Result<Vec<Question>, DbError> {
        self.query_questions_by_uuid(QUERY_VOTED_QUESTIONS, user_id)
            .await
    }

    pub async fn query_created_questions(&self, user_id: &Uuid) -> Result<Vec<Question>, DbError> {
        self.query_questions_by_uuid(QUERY_CREATED_QUESTIONS, user_id)
            .await
    }
}
