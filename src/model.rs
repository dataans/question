use std::convert::TryFrom;

use deadpool_postgres::tokio_postgres::Row;
use time::OffsetDateTime;
use uuid::Uuid;

use crate::api::types::answer::VoteType;

pub struct Question {
    pub pk: Uuid,
    pub id: String,
    pub title: String,
    pub text: String,
    pub created_at: OffsetDateTime,
    pub updated_at: OffsetDateTime,
    pub creator_id: Uuid,
    pub views: i32,
    pub sample_id: Option<Uuid>,
}

impl TryFrom<Row> for Question {
    type Error = postgres::Error;

    fn try_from(row: Row) -> Result<Self, Self::Error> {
        Ok(Self {
            pk: row.try_get(0)?,
            id: row.try_get(1)?,
            title: row.try_get(2)?,
            text: row.try_get(3)?,
            created_at: row.try_get(4)?,
            updated_at: row.try_get(5)?,
            creator_id: row.try_get(6)?,
            views: row.try_get(7)?,
            sample_id: row.try_get(8)?,
        })
    }
}

pub struct Tag {
    pub pk: Uuid,
    pub id: String,
    pub created_at: OffsetDateTime,
    pub name: String,
    pub creator_id: Uuid,
}

impl TryFrom<Row> for Tag {
    type Error = postgres::Error;

    fn try_from(row: Row) -> Result<Self, Self::Error> {
        Ok(Self {
            pk: row.try_get(0)?,
            id: row.try_get(1)?,
            name: row.try_get(2)?,
            created_at: row.try_get(3)?,
            creator_id: row.try_get(4)?,
        })
    }
}

pub struct QuestionToUser {
    #[allow(dead_code)]
    question_id: Uuid,
    #[allow(dead_code)]
    user_id: Uuid,
}

impl TryFrom<Row> for QuestionToUser {
    type Error = postgres::Error;

    fn try_from(row: Row) -> Result<Self, Self::Error> {
        Ok(Self {
            question_id: row.try_get(0)?,
            user_id: row.try_get(1)?,
        })
    }
}

pub type Vote = QuestionToUser;
pub type SavedQuestion = QuestionToUser;

#[derive(Debug)]
pub struct Answer {
    pub id: Uuid,
    pub text: String,
    pub created_at: OffsetDateTime,
    pub updated_at: OffsetDateTime,
    pub creator_id: Uuid,
    pub sample_id: Option<Uuid>,
}

impl TryFrom<Row> for Answer {
    type Error = postgres::Error;

    fn try_from(row: Row) -> Result<Self, Self::Error> {
        Ok(Self {
            id: row.try_get(0)?,
            text: row.try_get(1)?,
            created_at: row.try_get(2)?,
            updated_at: row.try_get(3)?,
            creator_id: row.try_get(4)?,
            sample_id: row.try_get(5)?,
        })
    }
}

#[derive(PartialEq, Eq)]
pub enum AnswerVoteType {
    Up,
    Down,
}

impl From<i16> for AnswerVoteType {
    fn from(value: i16) -> Self {
        if value > 0 {
            AnswerVoteType::Up
        } else {
            AnswerVoteType::Down
        }
    }
}

impl From<&AnswerVoteType> for i16 {
    fn from(vote_type: &AnswerVoteType) -> Self {
        match vote_type {
            AnswerVoteType::Up => 1,
            AnswerVoteType::Down => -1,
        }
    }
}

impl From<&VoteType> for AnswerVoteType {
    fn from(vote_type: &VoteType) -> Self {
        match vote_type {
            VoteType::Up => AnswerVoteType::Up,
            VoteType::Down => AnswerVoteType::Down,
        }
    }
}

pub struct AnswerVote {
    pub answer_id: Uuid,
    pub user_id: Uuid,
    pub vote_type: AnswerVoteType,
}

impl TryFrom<Row> for AnswerVote {
    type Error = postgres::Error;

    fn try_from(row: Row) -> Result<Self, Self::Error> {
        let vote_type: i16 = row.try_get(2)?;

        Ok(Self {
            answer_id: row.try_get(0)?,
            user_id: row.try_get(1)?,
            vote_type: vote_type.into(),
        })
    }
}
