use std::sync::Arc;

use actix_cors::Cors;
use actix_web::{web, App, HttpServer};
use async_mutex::Mutex;
use deadpool_postgres::{tokio_postgres::NoTls, Runtime};
use session_manager::SessionService;

use crate::{
    api::handlers::{
        answer::{
            create_answer_comment, delete_answer_vote, get_answer_by_id, update_answer, vote_answer, get_created_answers, get_voted_answers,
        },
        default_route,
        question::{
            add_answer, add_question, add_to_saved, create_question_comment, get_question_answers,
            get_question_by_id, health, update_question, vote_question, get_saved_questions, get_voted_questions, get_created_questions,
        },
        root_health,
        tag::{create_tag, get_tag, get_tag_info, get_tag_questions, get_tags},
    },
    config::{bind_address, check_env_vars, pool_config},
    db::{answer::AnswerRepository, question::QuestionRepository, tag::TagRepository},
    grpc::{comment::CommentGrpcClient, user::UserGrpcClient},
    logging::setup_logger,
    services::{answer::AnswerService, question::QuestionService, tag::TagService},
};

pub struct AppData {
    pub session_service: Mutex<SessionService>,
    pub question_service: Arc<Mutex<QuestionService>>,
    pub answer_service: Mutex<AnswerService>,
    pub tag_service: Arc<TagService>,
}

impl AppData {
    pub fn new() -> Self {
        let pool = Arc::new(Mutex::new(
            pool_config()
                .create_pool(Some(Runtime::Tokio1), NoTls)
                .unwrap(),
        ));

        let user_grpc_client = Arc::new(Mutex::new(UserGrpcClient::new()));
        let comment_grpc_client =
            Arc::new(Mutex::new(CommentGrpcClient::new(user_grpc_client.clone())));

        let tag_service = Arc::new(TagService::new(
            TagRepository::new(pool.clone()),
            user_grpc_client.clone(),
        ));

        let question_service = Arc::new(Mutex::new(QuestionService::new(
            QuestionRepository::new(pool.clone()),
            comment_grpc_client.clone(),
            user_grpc_client.clone(),
            tag_service.clone(),
        )));

        Self {
            session_service: Mutex::new(SessionService::new_from_env()),
            question_service: question_service.clone(),
            answer_service: Mutex::new(AnswerService::new(
                AnswerRepository::new(pool.clone()),
                comment_grpc_client,
                user_grpc_client,
                question_service,
            )),
            tag_service,
        }
    }
}

impl Default for AppData {
    fn default() -> Self {
        Self::new()
    }
}

#[allow(deprecated)]
pub async fn start_app() -> std::io::Result<()> {
    check_env_vars();
    setup_logger();

    HttpServer::new(move || {
        App::new()
            .wrap(actix_web::middleware::Logger::default())
            .wrap(
                Cors::default()
                    .expose_any_header()
                    .supports_credentials()
                    .allow_any_header()
                    .allow_any_origin()
                    .allow_any_method(),
            )
            .default_service(actix_web::web::route().to(default_route))
            .data(AppData::new())
            .service(root_health)
            .service(
                web::scope("/api/v1/question")
                    .service(health)
                    .service(add_question)
                    .service(add_answer)
                    .service(get_question_by_id)
                    .service(add_to_saved)
                    .service(vote_question)
                    .service(create_question_comment)
                    .service(get_question_answers)
                    .service(update_question)
                    .service(get_saved_questions)
                    .service(get_voted_questions)
                    .service(get_created_questions),
            )
            .service(
                web::scope("/api/v1/answer")
                    .service(get_answer_by_id)
                    .service(create_answer_comment)
                    .service(vote_answer)
                    .service(delete_answer_vote)
                    .service(update_answer)
                    .service(get_created_answers)
                    .service(get_voted_answers),
            )
            .service(
                web::scope("/api/v1/tag")
                    .service(create_tag)
                    .service(get_tag)
                    .service(get_tags)
                    .service(get_tag_info)
                    .service(get_tag_questions),
            )
    })
    .bind(bind_address())?
    .workers(4)
    .run()
    .await
}
