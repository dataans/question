#[actix_web::main]
async fn main() -> std::io::Result<()> {
    question::app::start_app().await
}
