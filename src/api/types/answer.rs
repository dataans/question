use actix_web::{http::StatusCode, HttpRequest, HttpResponse, Responder};
use serde::{Deserialize, Serialize};
use time::{serde::rfc3339, OffsetDateTime};
use uuid::Uuid;

use crate::model::AnswerVoteType;

use super::{CommentResponse, UserResponse};

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AnswerCreate {
    pub text: String,
    pub sample_id: Option<Uuid>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AnswerUpdate {
    pub id: Uuid,
    pub text: String,
    pub sample_id: Option<Uuid>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AnswerResponse {
    pub id: Uuid,
    pub text: String,
    #[serde(with = "rfc3339")]
    pub created_at: OffsetDateTime,
    #[serde(with = "rfc3339")]
    pub updated_at: OffsetDateTime,
    pub creator: UserResponse,
    pub sample_id: Option<Uuid>,
    pub comments: Vec<CommentResponse>,
    pub votes: i64,

    pub is_voted: Option<VoteType>,
}

impl Responder for AnswerResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self).unwrap())
    }
}

pub struct AnswersResponse(pub Vec<AnswerResponse>);

impl Responder for AnswersResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self.0).unwrap())
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum VoteType {
    Up,
    Down,
}

impl From<AnswerVoteType> for VoteType {
    fn from(vote_type: AnswerVoteType) -> Self {
        match vote_type {
            AnswerVoteType::Up => VoteType::Up,
            AnswerVoteType::Down => VoteType::Down,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AnswerVoteCreate {
    pub vote_type: VoteType,
}

#[cfg(test)]
mod tests {
    use crate::api::types::answer::{AnswerVoteCreate, VoteType};

    #[test]
    fn vote_serialize() {
        let vote = AnswerVoteCreate {
            vote_type: VoteType::Down,
        };
        println!("{}", serde_json::to_string(&vote).unwrap());
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SimpleAnswerResponse {
    pub id: Uuid,
    pub text: String,
    #[serde(with = "rfc3339")]
    pub created_at: OffsetDateTime,
    #[serde(with = "rfc3339")]
    pub updated_at: OffsetDateTime,
    pub creator_id: Uuid,
    pub sample_id: Option<Uuid>,
    pub votes: i64,

    pub is_voted: Option<VoteType>,
}

impl Responder for SimpleAnswerResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self).unwrap())
    }
}

pub struct SimpleAnswersResponse(pub Vec<SimpleAnswerResponse>);

impl Responder for SimpleAnswersResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self.0).unwrap())
    }
}
