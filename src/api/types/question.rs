use actix_web::{http::StatusCode, HttpRequest, HttpResponse, Responder};
use serde::{Deserialize, Serialize};
use time::{serde::rfc3339, OffsetDateTime};
use uuid::Uuid;

use crate::model::Question;

use super::{tag::TagResponse, CommentResponse, UserResponse};

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct QuestionCreate {
    pub title: String,
    pub text: String,
    pub sample_id: Option<Uuid>,
    pub tags: Vec<String>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct QuestionUpdate {
    pub id: u64,
    pub title: Option<String>,
    pub text: Option<String>,
    pub tags: Option<Vec<String>>,
    pub sample_id: Option<Uuid>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct QuestionResponse {
    pub id: String,
    pub title: String,
    pub text: String,
    #[serde(with = "rfc3339")]
    pub created_at: OffsetDateTime,
    #[serde(with = "rfc3339")]
    pub updated_at: OffsetDateTime,
    pub creator: UserResponse,
    pub up_votes: i64,
    pub saved_times: i64,
    pub views: i32,
    pub sample_id: Option<Uuid>,
    pub tags: Vec<TagResponse>,
    pub comments: Vec<CommentResponse>,

    pub is_voted: bool,
    pub is_saved: bool,
}

impl QuestionResponse {
    pub fn set_up_votes(&mut self, up_votes: i64) {
        self.up_votes = up_votes;
    }

    pub fn set_saved_times(&mut self, saved_times: i64) {
        self.saved_times = saved_times;
    }

    pub fn set_is_saved(&mut self, is_saved: bool) {
        self.is_saved = is_saved;
    }

    pub fn set_is_voted(&mut self, is_voted: bool) {
        self.is_voted = is_voted;
    }

    pub fn set_tags(&mut self, tags: Vec<TagResponse>) {
        self.tags = tags;
    }

    pub fn set_comments(&mut self, comments: Vec<CommentResponse>) {
        self.comments = comments;
    }
}

impl Responder for QuestionResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self).unwrap())
    }
}

pub struct QuestionsResponse(pub Vec<QuestionResponse>);

impl Responder for QuestionsResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self.0).unwrap())
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SimpleQuestionResponse {
    pub id: String,
    pub title: String,
    pub text: String,
    #[serde(with = "rfc3339")]
    pub created_at: OffsetDateTime,
    #[serde(with = "rfc3339")]
    pub updated_at: OffsetDateTime,
    pub creator_id: Uuid,
    pub views: i32,
    pub sample_id: Option<Uuid>,
}

impl From<Question> for SimpleQuestionResponse {
    fn from(questions: Question) -> Self {
        let Question {
            pk: _,
            id,
            title,
            text,
            created_at,
            updated_at,
            creator_id,
            views,
            sample_id,
        } = questions;

        Self {
            id,
            title,
            text,
            created_at,
            updated_at,
            creator_id,
            views,
            sample_id,
        }
    }
}

impl Responder for SimpleQuestionResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self).unwrap())
    }
}

pub struct SimpleQuestionsResponse(pub Vec<SimpleQuestionResponse>);

impl Responder for SimpleQuestionsResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self.0).unwrap())
    }
}
