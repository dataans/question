use actix_web::{http::StatusCode, HttpRequest, HttpResponse, Responder};
use serde::{Deserialize, Serialize};
use time::{serde::rfc3339, OffsetDateTime};
use uuid::Uuid;

use crate::model::Tag;

use super::UserResponse;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct TagCreate {
    pub name: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct TagResponse {
    pub id: String,
    pub name: String,
    #[serde(with = "rfc3339")]
    pub created_at: OffsetDateTime,
    pub creator_id: Uuid,
}

impl From<Tag> for TagResponse {
    fn from(tag: Tag) -> Self {
        let Tag {
            id,
            name,
            created_at,
            creator_id,
            ..
        } = tag;
        Self {
            id,
            name,
            created_at,
            creator_id,
        }
    }
}

impl Responder for TagResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self).unwrap())
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct TagInfoResponse {
    pub id: String,
    pub name: String,
    #[serde(with = "rfc3339")]
    pub created_at: OffsetDateTime,
    pub creator: UserResponse,
    pub questions_amount: i64,
}

impl Responder for TagInfoResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self).unwrap())
    }
}
