use actix_web::{delete, get, post, put, web, HttpRequest, Responder};

use crate::{
    api::types::{
        answer::{AnswerUpdate, AnswerVoteCreate},
        CommentCreate,
    },
    app::AppData,
    services::{answer::AnswerError, question::QuestionError},
};

use super::{UuidId, AUTH_COOKIE_NAME};

#[get("/{id}")]
pub async fn get_answer_by_id(
    params: web::Path<UuidId>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let user_id = match req.cookie(AUTH_COOKIE_NAME) {
        Some(cookie) => app
            .session_service
            .lock()
            .await
            .get_session(cookie.value())
            .await
            .ok()
            .map(|session| session.user_id),
        None => None,
    };

    app.answer_service
        .lock()
        .await
        .find_by_id(&params.id, &user_id)
        .await
}

#[post("/{id}/comment")]
pub async fn create_answer_comment(
    params: web::Path<UuidId>,
    data: web::Json<CommentCreate>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| AnswerError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.answer_service
        .lock()
        .await
        .create_comment(&data, &params.id, &session.user_id)
        .await
}

#[post("/{id}/vote")]
pub async fn vote_answer(
    params: web::Path<UuidId>,
    data: web::Json<AnswerVoteCreate>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| QuestionError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.answer_service
        .lock()
        .await
        .vote(&data, &params.id, &session.user_id)
        .await
}

#[delete("/{id}/vote")]
pub async fn delete_answer_vote(
    params: web::Path<UuidId>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| QuestionError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.answer_service
        .lock()
        .await
        .delete_vote(&params.id, &session.user_id)
        .await
}

#[put("")]
pub async fn update_answer(
    data: web::Json<AnswerUpdate>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| QuestionError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.answer_service
        .lock()
        .await
        .update(data.into_inner(), &session.user_id)
        .await
}

#[get("/user/voted")]
pub async fn get_voted_answers(
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| QuestionError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.answer_service
        .lock()
        .await
        .find_voted(&session.user_id)
        .await
}

#[get("/created/{id}")]
pub async fn get_created_answers(
    params: web::Path<UuidId>,
    app: web::Data<AppData>,
) -> impl Responder {
    app.answer_service
        .lock()
        .await
        .find_created(&params.id)
        .await
}
