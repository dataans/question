use actix_web::{get, post, put, web, HttpRequest, HttpResponse, Responder};

use crate::{
    api::{
        handlers::{Id, AUTH_COOKIE_NAME, UuidId},
        types::{
            answer::AnswerCreate,
            question::{QuestionCreate, QuestionUpdate},
            CommentCreate,
        },
    },
    app::AppData,
    services::{answer::AnswerError, question::QuestionError},
};

#[get("/health")]
pub async fn health() -> impl Responder {
    HttpResponse::Ok().body("question ok.")
}

#[get("/{id}")]
pub async fn get_question_by_id(
    params: web::Path<Id>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let user_id = match req.cookie(AUTH_COOKIE_NAME) {
        Some(cookie) => app
            .session_service
            .lock()
            .await
            .get_session(cookie.value())
            .await
            .ok()
            .map(|session| session.user_id),
        None => None,
    };

    app.question_service
        .lock()
        .await
        .find_by_id(&params.id, &user_id)
        .await
}

#[post("/{id}/vote")]
pub async fn vote_question(
    params: web::Path<Id>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| QuestionError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.question_service
        .lock()
        .await
        .vote(&params.id, &session.user_id)
        .await
}

#[post("/{id}/save")]
pub async fn add_to_saved(
    params: web::Path<Id>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| QuestionError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.question_service
        .lock()
        .await
        .add_to_saved(&params.id, &session.user_id)
        .await
}

#[post("/{id}/comment")]
pub async fn create_question_comment(
    params: web::Path<Id>,
    data: web::Json<CommentCreate>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| QuestionError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.question_service
        .lock()
        .await
        .create_comment(&data, &params.id, &session.user_id)
        .await
}

#[put("")]
pub async fn update_question(
    data: web::Json<QuestionUpdate>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| QuestionError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.question_service
        .lock()
        .await
        .update(data.into_inner(), &session.user_id)
        .await
}

#[post("")]
pub async fn add_question(
    data: web::Json<QuestionCreate>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| QuestionError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.question_service
        .lock()
        .await
        .create(&data, &session.user_id)
        .await
}

#[post("/{id}/answer")]
pub async fn add_answer(
    params: web::Path<Id>,
    data: web::Json<AnswerCreate>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| AnswerError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.answer_service
        .lock()
        .await
        .create(&data, &params.id, &session.user_id)
        .await
}

#[get("/{id}/answer")]
pub async fn get_question_answers(
    params: web::Path<Id>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let user_id = match req.cookie(AUTH_COOKIE_NAME) {
        Some(cookie) => app
            .session_service
            .lock()
            .await
            .get_session(cookie.value())
            .await
            .ok()
            .map(|session| session.user_id),
        None => None,
    };

    app.answer_service
        .lock()
        .await
        .get_answers(&params.id, &user_id)
        .await
}

#[get("/user/saved")]
pub async fn get_saved_questions(
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| QuestionError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.question_service
        .lock()
        .await
        .find_saved(&session.user_id)
        .await
}

#[get("/user/voted")]
pub async fn get_voted_questions(
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| QuestionError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.question_service
        .lock()
        .await
        .find_voted(&session.user_id)
        .await
}

#[get("/created/{id}")]
pub async fn get_created_questions(
    params: web::Path<UuidId>,
    app: web::Data<AppData>,
) -> impl Responder {
    app.question_service
        .lock()
        .await
        .find_created(&params.id)
        .await
}
