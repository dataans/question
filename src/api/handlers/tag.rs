use actix_web::{
    get, http::StatusCode, post, web, HttpRequest, HttpResponse, Responder, ResponseError,
};

use crate::{
    api::{
        handlers::{Id, AUTH_COOKIE_NAME},
        types::tag::TagCreate,
    },
    app::AppData,
    services::tag::TagError,
};

#[post("")]
pub async fn create_tag(
    data: web::Json<TagCreate>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| TagError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.tag_service.create(&data, &session.user_id).await
}

#[get("")]
pub async fn get_tags(app: web::Data<AppData>) -> HttpResponse<String> {
    match app.tag_service.get_all_tags().await {
        Ok(tags) => {
            HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&tags).unwrap())
        }
        Err(err) => HttpResponse::<String>::with_body(err.status_code(), err.to_string()),
    }
}

#[get("/{id}")]
pub async fn get_tag(params: web::Path<Id>, app: web::Data<AppData>) -> impl Responder {
    app.tag_service.find_by_id(&params.id).await
}

#[get("/{id}/info")]
pub async fn get_tag_info(params: web::Path<Id>, app: web::Data<AppData>) -> impl Responder {
    app.tag_service.get_tag_info(&params.id).await
}

#[get("/{id}/question")]
pub async fn get_tag_questions(
    params: web::Path<Id>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let user_id = match req.cookie(AUTH_COOKIE_NAME) {
        Some(cookie) => app
            .session_service
            .lock()
            .await
            .get_session(cookie.value())
            .await
            .ok()
            .map(|session| session.user_id),
        None => None,
    };

    app.question_service
        .lock()
        .await
        .find_questions_by_tag(&params.id, &user_id)
        .await
}
