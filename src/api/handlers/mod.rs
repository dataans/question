pub mod answer;
pub mod question;
pub mod tag;

use actix_web::{get, http::StatusCode, HttpResponse, Responder};
use serde::Deserialize;
use uuid::Uuid;

const AUTH_COOKIE_NAME: &str = "SessionId";

#[derive(Deserialize)]
pub struct Id {
    pub id: String,
}

#[derive(Deserialize)]
pub struct UuidId {
    pub id: Uuid,
}

pub async fn default_route() -> impl Responder {
    HttpResponse::Ok().status(StatusCode::NOT_FOUND).body(())
}

#[get("/")]
pub async fn root_health() -> impl Responder {
    HttpResponse::Ok().body("root: ok.")
}
