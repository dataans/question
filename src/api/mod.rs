pub mod handlers;
pub mod types;

use actix_web::{
    body::BoxBody,
    cookie::{time::Duration, Cookie},
    http::StatusCode,
    HttpRequest, HttpResponse, Responder,
};
use time::OffsetDateTime;

pub const DAYS_SESSION_VALID: i64 = 3;

pub struct EmptyResponse {
    code: StatusCode,
    cookies: Vec<(String, String)>,
}

impl EmptyResponse {
    pub fn new(code: StatusCode) -> Self {
        Self {
            code,
            cookies: Vec::new(),
        }
    }

    pub fn with_cookie(self, name: String, value: String) -> Self {
        let EmptyResponse { code, mut cookies } = self;
        cookies.push((name, value));
        Self { code, cookies }
    }
}

impl Responder for EmptyResponse {
    type Body = BoxBody;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        let EmptyResponse { code, cookies } = self;

        let mut response = HttpResponse::Ok();
        let response_builder = response.status(code);

        for (name, value) in cookies {
            response_builder.cookie(
                Cookie::build(name, value)
                    .http_only(true)
                    .path("/")
                    .expires(
                        OffsetDateTime::now_utc().checked_add(Duration::days(DAYS_SESSION_VALID)),
                    )
                    .finish(),
            );
        }

        response_builder.body(())
    }
}
