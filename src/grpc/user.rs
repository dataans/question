use std::{env, str::FromStr};

use tonic::{transport::Channel, Status};
use uuid::Uuid;

use crate::{
    api::types::UserResponse,
    config::USER_GRPC_SERVER_URL_ENV,
    generated::user::{user_service_client::UserServiceClient, User, UsersIds},
};

pub struct UserGrpcClient {
    client: Option<UserServiceClient<Channel>>,
}

impl TryFrom<User> for UserResponse {
    type Error = uuid::Error;

    fn try_from(user: User) -> Result<Self, Self::Error> {
        let User {
            id,
            username,
            full_name,
            avatar_url,
            ..
        } = user;
        Ok(Self {
            id: Uuid::from_str(&id)?,
            username,
            full_name,
            avatar_url,
        })
    }
}

impl UserGrpcClient {
    pub fn new() -> Self {
        Self { client: None }
    }

    pub async fn get_users(&mut self, ids: &[Uuid]) -> Result<Vec<UserResponse>, Status> {
        if self.client.is_none() {
            self.client = Some(
                UserServiceClient::connect(env::var(USER_GRPC_SERVER_URL_ENV).unwrap())
                    .await
                    .map_err(|err| Status::internal(format!("{:?}", err)))?,
            );
        }

        let raw_users = self
            .client
            .as_mut()
            .unwrap()
            .get_users(UsersIds {
                ids: ids.iter().map(|id| id.to_string()).collect(),
            })
            .await?
            .into_inner()
            .users;

        let mut users = Vec::with_capacity(raw_users.len());

        for user in raw_users {
            users.push(
                user.try_into()
                    .map_err(|err| Status::internal(format!("{:?}", err)))?,
            );
        }

        Ok(users)
    }
}

impl Default for UserGrpcClient {
    fn default() -> Self {
        Self::new()
    }
}
