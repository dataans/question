use std::{env, str::FromStr, sync::Arc};

use async_mutex::Mutex;
use time::OffsetDateTime;
use tonic::{transport::Channel, Status};
use uuid::Uuid;

use super::user::UserGrpcClient;
use crate::{
    api::types::{CommentCreate, CommentResponse},
    config::COMMENT_GRPC_SERVER_URL_ENV,
    generated::comment::{
        comment_service_client::CommentServiceClient, Comment, CommentCreate as GrpcCommentCreate,
        CommentsOptions, Parent,
    },
};

pub enum CommentType {
    Question = 0,
    Answer = 1,
}

impl Into<i32> for CommentType {
    fn into(self) -> i32 {
        match self {
            CommentType::Question => 0,
            CommentType::Answer => 1,
        }
    }
}

#[derive(Clone)]
pub struct CommentGrpcClient {
    client: Option<CommentServiceClient<Channel>>,
    user_client: Arc<Mutex<UserGrpcClient>>,
}

impl CommentGrpcClient {
    pub fn new(user_client: Arc<Mutex<UserGrpcClient>>) -> Self {
        Self {
            client: None,
            user_client,
        }
    }

    pub async fn create_comment(
        &mut self,
        comment_data: &CommentCreate,
        comment_type: CommentType,
        parent_id: &Uuid,
        creator_id: &Uuid,
    ) -> Result<CommentResponse, Status> {
        if self.client.is_none() {
            self.client = Some(
                CommentServiceClient::connect(env::var(COMMENT_GRPC_SERVER_URL_ENV).unwrap())
                    .await
                    .map_err(|err| Status::internal(format!("{:?}", err)))?,
            );
        }

        let Comment {
            id,
            text,
            creator_id,
            created_at,
            updated_at,
            up_votes,
            is_voted,
            ..
        } = self
            .client
            .as_mut()
            .unwrap()
            .create_comment(GrpcCommentCreate {
                text: comment_data.text.clone(),
                creator_id: creator_id.to_string(),
                sample_id: None,
                parent: Some(Parent {
                    comment_type: comment_type.into(),
                    parent_id: parent_id.to_string(),
                }),
            })
            .await?
            .into_inner();

        let creator_id = Uuid::from_str(&creator_id)
            .map_err(|err| Status::internal(format!("bad uuid: {:?}", err)))?;

        let creator = self
            .user_client
            .lock()
            .await
            .get_users(&[creator_id])
            .await?
            .into_iter()
            .next()
            .unwrap();

        let created_at = OffsetDateTime::from_unix_timestamp(created_at.unwrap().seconds)
            .map_err(|err| Status::internal(format!("bad timestamp: {:?}", err)))?;
        let updated_at = OffsetDateTime::from_unix_timestamp(updated_at.unwrap().seconds)
            .map_err(|err| Status::internal(format!("bad timestamp: {:?}", err)))?;

        Ok(CommentResponse {
            id: Uuid::from_str(&id)
                .map_err(|err| Status::internal(format!("bad uuid: {:?}", err)))?,
            text,
            creator,
            created_at,
            updated_at,
            up_votes,
            is_voted,
        })
    }

    pub async fn get_comments(
        &mut self,
        parent_id: &Uuid,
        user_id: &Option<Uuid>,
        comment_type: CommentType,
    ) -> Result<Vec<CommentResponse>, Status> {
        if self.client.is_none() {
            self.client = Some(
                CommentServiceClient::connect(env::var(COMMENT_GRPC_SERVER_URL_ENV).unwrap())
                    .await
                    .map_err(|err| Status::internal(format!("{:?}", err)))?,
            );
        }

        let raw_comments = self
            .client
            .as_mut()
            .unwrap()
            .get_comments(CommentsOptions {
                parent: Some(Parent {
                    comment_type: comment_type.into(),
                    parent_id: parent_id.to_string(),
                }),
                user_id: user_id.map(|id| id.to_string()),
            })
            .await?
            .into_inner()
            .comments;

        let mut users_ids = Vec::with_capacity(raw_comments.len());
        for comment in raw_comments.iter() {
            users_ids.push(
                Uuid::from_str(&comment.creator_id)
                    .map_err(|err| Status::internal(format!("{:?}", err)))?,
            );
        }

        let raw_users = self.user_client.lock().await.get_users(&users_ids).await?;

        let mut comments = Vec::with_capacity(raw_comments.len());
        for (comment, creator) in raw_comments.into_iter().zip(raw_users.into_iter()) {
            let Comment {
                id,
                text,
                created_at,
                updated_at,
                up_votes,
                is_voted,
                ..
            } = comment;
            let created_at =
                OffsetDateTime::from_unix_timestamp(created_at.unwrap().seconds).unwrap();
            let updated_at =
                OffsetDateTime::from_unix_timestamp(updated_at.unwrap().seconds).unwrap();

            comments.push(CommentResponse {
                id: Uuid::from_str(&id).map_err(|err| Status::internal(format!("{:?}", err)))?,
                text,
                creator,
                created_at,
                updated_at,
                up_votes,
                is_voted,
            })
        }

        Ok(comments)
    }
}
