use std::{hash::Hasher, sync::Arc};

use actix_web::{http::StatusCode, ResponseError};
use async_mutex::Mutex;
use fasthash::{lookup3::Hasher32, FastHasher};
use log::error;
use session_manager::SessionError;
use thiserror::Error;
use time::OffsetDateTime;
use tonic::Status;
use uuid::Uuid;

use super::tag::{TagError, TagService};
use crate::{
    api::types::{
        question::{
            QuestionCreate, QuestionResponse, QuestionUpdate, QuestionsResponse,
            SimpleQuestionsResponse,
        },
        CommentCreate, CommentResponse,
    },
    db::{question::QuestionRepository, DbError},
    grpc::{
        comment::{CommentGrpcClient, CommentType},
        user::UserGrpcClient,
    },
    model::Question,
};

#[derive(Error, Debug)]
pub enum QuestionError {
    #[error("Internal server error: {0:?}")]
    InternalDbError(#[from] DbError),
    #[error("Authentication error: {0}")]
    NotAuthorized(String),
    #[error("Internal server error: {0}")]
    Internal(String),
    #[error("Tag error: {0:?}")]
    Tag(#[from] TagError),
    #[error("Grpc error: {0:?}")]
    GrpcError(String),
    #[error("Question with id {0} not found")]
    NotFound(String),
    #[error("Permission denied: {0}")]
    PermissionDenied(String),
}

impl From<SessionError> for QuestionError {
    fn from(e: SessionError) -> Self {
        match e {
            SessionError::SessionToken(error) => QuestionError::NotAuthorized(error),
            SessionError::Crypto(error) => QuestionError::Internal(error),
            SessionError::RedisConnection(error) => QuestionError::Internal(format!("{}", error)),
        }
    }
}

impl From<Status> for QuestionError {
    fn from(err: Status) -> Self {
        error!(
            "Grpc error: {}. Metadata: {:?}",
            err.message(),
            err.metadata()
        );
        Self::GrpcError(format!("{:?}", err.code()))
    }
}

impl ResponseError for QuestionError {
    fn status_code(&self) -> StatusCode {
        match self {
            QuestionError::NotAuthorized(_) => StatusCode::UNAUTHORIZED,
            QuestionError::Tag(e) => e.status_code(),
            QuestionError::NotFound(_) => StatusCode::NOT_FOUND,
            QuestionError::PermissionDenied(_) => StatusCode::FORBIDDEN,
            e => {
                log::error!("{:?}", e);
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    }
}

pub struct QuestionService {
    question_repository: QuestionRepository,
    comment_grpc_client: Arc<Mutex<CommentGrpcClient>>,
    user_grpc_client: Arc<Mutex<UserGrpcClient>>,
    tag_service: Arc<TagService>,
}

impl QuestionService {
    pub fn new(
        question_repository: QuestionRepository,
        comment_grpc_client: Arc<Mutex<CommentGrpcClient>>,
        user_grpc_client: Arc<Mutex<UserGrpcClient>>,
        tag_service: Arc<TagService>,
    ) -> Self {
        Self {
            question_repository,
            comment_grpc_client,
            user_grpc_client,
            tag_service,
        }
    }

    pub async fn create(
        &mut self,
        data: &QuestionCreate,
        user_id: &Uuid,
    ) -> Result<QuestionResponse, QuestionError> {
        let QuestionCreate {
            title,
            text,
            sample_id,
            ..
        } = data.clone();

        let pk = Uuid::new_v4();

        let mut hasher = Hasher32::new();
        hasher.write(pk.to_string().as_bytes());

        let question = Question {
            pk,
            id: hasher.finish().to_string(),
            title,
            text,
            creator_id: *user_id,
            updated_at: OffsetDateTime::now_utc(),
            created_at: OffsetDateTime::now_utc(),
            views: 0,
            sample_id,
        };

        self.question_repository.save(&question).await?;

        for tag_id in data.tags.iter() {
            self.tag_service
                .add_tag_to_question(&question.pk, tag_id)
                .await?;
        }

        Ok(self.find_by_id(&question.id, &Some(*user_id)).await?)
    }

    pub async fn get_question(&self, question_id: &str) -> Result<Question, QuestionError> {
        self.question_repository
            .find_by_id(question_id)
            .await?
            .ok_or_else(|| QuestionError::NotFound(question_id.into()))
    }

    pub async fn find_by_id(
        &mut self,
        id: &str,
        user_id: &Option<Uuid>,
    ) -> Result<QuestionResponse, QuestionError> {
        let Question {
            pk,
            id,
            title,
            text,
            created_at,
            updated_at,
            creator_id,
            views,
            sample_id,
        } = match self.question_repository.find_by_id(id).await? {
            Some(question) => question,
            None => return Err(QuestionError::NotFound(id.into())),
        };

        let creators = self
            .user_grpc_client
            .lock()
            .await
            .get_users(&[creator_id])
            .await?;

        let up_votes = self.question_repository.count_votes_by_id(&pk).await?;

        let saved_times = self.question_repository.count_saved_questions(&pk).await?;
        let tags = self.tag_service.question_tags(&pk).await?;

        let (is_voted, is_saved) = if let Some(user_id) = user_id {
            (
                self.question_repository
                    .find_vote(user_id, &pk)
                    .await?
                    .is_some(),
                self.question_repository
                    .find_in_saved(user_id, &pk)
                    .await?
                    .is_some(),
            )
        } else {
            (false, false)
        };

        let comments = self
            .comment_grpc_client
            .lock()
            .await
            .get_comments(&pk, user_id, CommentType::Question)
            .await?;

        Ok(QuestionResponse {
            id,
            title,
            text,
            created_at,
            updated_at,
            creator: creators.get(0).unwrap().clone(),
            up_votes,
            saved_times,
            views,
            sample_id,
            tags,
            comments,
            is_voted,
            is_saved,
        })
    }

    pub async fn vote(&self, question_id: &str, user_id: &Uuid) -> Result<String, QuestionError> {
        let question_id = self
            .question_repository
            .find_by_id(question_id)
            .await?
            .map(|q| q.pk)
            .ok_or_else(|| QuestionError::NotFound(question_id.into()))?;

        if self
            .question_repository
            .find_vote(user_id, &question_id)
            .await?
            .is_some()
        {
            self.question_repository
                .delete_vote(user_id, &question_id)
                .await?;
        } else {
            self.question_repository
                .add_vote(user_id, &question_id)
                .await?;
        }

        Ok(self
            .question_repository
            .count_votes_by_id(&question_id)
            .await?
            .to_string())
    }

    pub async fn add_to_saved(
        &self,
        question_id: &str,
        user_id: &Uuid,
    ) -> Result<String, QuestionError> {
        let question_id = self
            .question_repository
            .find_by_id(question_id)
            .await?
            .map(|q| q.pk)
            .ok_or_else(|| QuestionError::NotFound(question_id.into()))?;

        if self
            .question_repository
            .find_in_saved(user_id, &question_id)
            .await?
            .is_some()
        {
            self.question_repository
                .delete_from_saved(user_id, &question_id)
                .await?;
        } else {
            self.question_repository
                .add_to_saved(user_id, &question_id)
                .await?;
        }

        Ok(self
            .question_repository
            .count_votes_by_id(&question_id)
            .await?
            .to_string())
    }

    pub async fn create_comment(
        &mut self,
        comment_data: &CommentCreate,
        question_id: &str,
        user_id: &Uuid,
    ) -> Result<CommentResponse, QuestionError> {
        let question = self
            .question_repository
            .find_by_id(question_id)
            .await?
            .ok_or_else(|| QuestionError::NotFound(question_id.into()))?;

        Ok(self
            .comment_grpc_client
            .lock()
            .await
            .create_comment(comment_data, CommentType::Question, &question.pk, user_id)
            .await?)
    }

    pub async fn add_question_answer(
        &self,
        question_id: &str,
        answer_id: &Uuid,
    ) -> Result<(), QuestionError> {
        let question = self
            .question_repository
            .find_by_id(question_id)
            .await?
            .ok_or_else(|| QuestionError::NotFound(question_id.into()))?;

        self.question_repository
            .add_question_answer(&question.pk, answer_id)
            .await?;

        Ok(())
    }

    pub async fn update(
        &mut self,
        data: QuestionUpdate,
        user_id: &Uuid,
    ) -> Result<QuestionResponse, QuestionError> {
        let QuestionUpdate {
            id,
            title,
            text,
            tags,
            sample_id,
        } = data;
        let id = id.to_string();

        let mut question = self
            .question_repository
            .find_by_id(&id)
            .await?
            .ok_or_else(|| QuestionError::NotFound(id.clone()))?;

        if question.creator_id != *user_id {
            return Err(QuestionError::PermissionDenied(
                "User can update only their created questions".into(),
            ));
        }

        if let Some(title) = title {
            question.title = title;
        }

        if let Some(text) = text {
            question.text = text;
        }

        question.sample_id = sample_id;
        question.updated_at = OffsetDateTime::now_utc();

        self.question_repository.update(&question).await?;

        if let Some(tags) = tags {
            self.tag_service.remove_question_tags(&question.pk).await?;

            for tag_id in tags {
                self.tag_service
                    .add_tag_to_question(&question.pk, &tag_id)
                    .await?;
            }
        }

        self.find_by_id(&id, &Some(*user_id)).await
    }

    pub async fn find_questions_by_tag(
        &mut self,
        tag_id: &str,
        user_id: &Option<Uuid>,
    ) -> Result<QuestionsResponse, QuestionError> {
        let tag = self.tag_service.get_by_id(tag_id).await?;

        let raw_question = self
            .question_repository
            .query_questions_by_tag_id(&tag.pk)
            .await?;

        let mut questions = Vec::with_capacity(raw_question.len());

        for question in raw_question {
            questions.push(self.find_by_id(&question.id, user_id).await?);
        }

        Ok(QuestionsResponse(questions))
    }

    pub async fn find_saved(
        &self,
        user_id: &Uuid,
    ) -> Result<SimpleQuestionsResponse, QuestionError> {
        Ok(SimpleQuestionsResponse(
            self.question_repository
                .query_saved_questions(user_id)
                .await?
                .into_iter()
                .map(|q| q.into())
                .collect(),
        ))
    }

    pub async fn find_voted(
        &self,
        user_id: &Uuid,
    ) -> Result<SimpleQuestionsResponse, QuestionError> {
        Ok(SimpleQuestionsResponse(
            self.question_repository
                .query_voted_questions(user_id)
                .await?
                .into_iter()
                .map(|q| q.into())
                .collect(),
        ))
    }

    pub async fn find_created(
        &self,
        user_id: &Uuid,
    ) -> Result<SimpleQuestionsResponse, QuestionError> {
        Ok(SimpleQuestionsResponse(
            self.question_repository
                .query_created_questions(user_id)
                .await?
                .into_iter()
                .map(|q| q.into())
                .collect(),
        ))
    }
}
