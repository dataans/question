use std::sync::Arc;

use actix_web::{http::StatusCode, ResponseError};
use async_mutex::Mutex;
use log::error;
use session_manager::SessionError;
use thiserror::Error;
use time::OffsetDateTime;
use tonic::Status;
use uuid::Uuid;

use crate::{
    api::{
        types::{
            answer::{
                AnswerCreate, AnswerResponse, AnswerUpdate, AnswerVoteCreate, AnswersResponse,
                SimpleAnswerResponse, SimpleAnswersResponse, VoteType,
            },
            CommentCreate, CommentResponse,
        },
        EmptyResponse,
    },
    db::{answer::AnswerRepository, DbError},
    grpc::{
        comment::{CommentGrpcClient, CommentType},
        user::UserGrpcClient,
    },
    model::{Answer, AnswerVote, AnswerVoteType},
};

use super::question::{QuestionError, QuestionService};

#[derive(Error, Debug)]
pub enum AnswerError {
    #[error("Internal server error: {0:?}")]
    InternalDbError(#[from] DbError),
    #[error("Authentication error: {0}")]
    NotAuthorized(String),
    #[error("Internal server error: {0}")]
    Internal(String),
    #[error("Grpc error: {0:?}")]
    GrpcError(String),
    #[error("Answer with id {0:?} not found")]
    NotFound(Uuid),
    #[error("Question error: {0:?}")]
    Question(#[from] QuestionError),
    #[error("Permission denied: {0}")]
    PermissionDenied(String),
}

impl From<SessionError> for AnswerError {
    fn from(e: SessionError) -> Self {
        match e {
            SessionError::SessionToken(error) => AnswerError::NotAuthorized(error),
            SessionError::Crypto(error) => AnswerError::Internal(error),
            SessionError::RedisConnection(error) => AnswerError::Internal(format!("{}", error)),
        }
    }
}

impl From<Status> for AnswerError {
    fn from(err: Status) -> Self {
        error!(
            "Grpc error: {}. Metadata: {:?}",
            err.message(),
            err.metadata()
        );
        Self::GrpcError(format!("{:?}", err.code()))
    }
}

impl ResponseError for AnswerError {
    fn status_code(&self) -> StatusCode {
        match self {
            AnswerError::NotAuthorized(_) => StatusCode::UNAUTHORIZED,
            AnswerError::NotFound(_) => StatusCode::NOT_FOUND,
            AnswerError::PermissionDenied(_) => StatusCode::FORBIDDEN,
            e => {
                log::error!("{:?}", e);
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    }
}

pub struct AnswerService {
    answer_repository: AnswerRepository,
    comment_grpc_client: Arc<Mutex<CommentGrpcClient>>,
    user_grpc_client: Arc<Mutex<UserGrpcClient>>,
    question_service: Arc<Mutex<QuestionService>>,
}

impl AnswerService {
    pub fn new(
        answer_repository: AnswerRepository,
        comment_grpc_client: Arc<Mutex<CommentGrpcClient>>,
        user_grpc_client: Arc<Mutex<UserGrpcClient>>,
        question_service: Arc<Mutex<QuestionService>>,
    ) -> Self {
        Self {
            answer_repository,
            comment_grpc_client,
            user_grpc_client,
            question_service,
        }
    }

    pub async fn create(
        &mut self,
        data: &AnswerCreate,
        question_id: &str,
        creator_id: &Uuid,
    ) -> Result<AnswerResponse, AnswerError> {
        let AnswerCreate { sample_id, text } = data;

        let answer = Answer {
            id: Uuid::new_v4(),
            text: text.into(),
            updated_at: OffsetDateTime::now_utc(),
            created_at: OffsetDateTime::now_utc(),
            creator_id: *creator_id,
            sample_id: *sample_id,
        };

        self.answer_repository.save(&answer).await?;

        self.question_service
            .lock()
            .await
            .add_question_answer(question_id, &answer.id)
            .await?;

        self.find_by_id(&answer.id, &Some(*creator_id)).await
    }

    pub async fn find_by_id(
        &mut self,
        answer_id: &Uuid,
        user_id: &Option<Uuid>,
    ) -> Result<AnswerResponse, AnswerError> {
        let Answer {
            id,
            text,
            created_at,
            updated_at,
            creator_id,
            sample_id,
        } = self
            .answer_repository
            .find_by_id(answer_id)
            .await?
            .ok_or_else(|| AnswerError::NotFound(*answer_id))?;

        let users = self
            .user_grpc_client
            .lock()
            .await
            .get_users(&[creator_id])
            .await?;
        let creator = users.get(0).unwrap();

        let comments = self
            .comment_grpc_client
            .lock()
            .await
            .get_comments(&id, user_id, CommentType::Answer)
            .await?;

        let votes = self.answer_repository.count_votes(answer_id).await?;

        let is_voted = match user_id {
            Some(user_id) => self
                .answer_repository
                .find_vote(&id, user_id)
                .await?
                .map(|vote| vote.vote_type.into()),
            None => None,
        };

        Ok(AnswerResponse {
            id,
            text,
            created_at,
            updated_at,
            creator: creator.clone(),
            sample_id,
            comments,
            votes,
            is_voted,
        })
    }

    pub async fn create_comment(
        &mut self,
        comment_data: &CommentCreate,
        comment_id: &Uuid,
        user_id: &Uuid,
    ) -> Result<CommentResponse, AnswerError> {
        let answer = self
            .answer_repository
            .find_by_id(comment_id)
            .await?
            .ok_or_else(|| AnswerError::NotFound(*comment_id))?;

        Ok(self
            .comment_grpc_client
            .lock()
            .await
            .create_comment(comment_data, CommentType::Answer, &answer.id, user_id)
            .await?)
    }

    pub async fn get_answers(
        &mut self,
        question_id: &str,
        user_id: &Option<Uuid>,
    ) -> Result<AnswersResponse, AnswerError> {
        let question = self
            .question_service
            .lock()
            .await
            .get_question(question_id)
            .await?;
        let question_id = question.pk;

        let answers = self
            .answer_repository
            .get_question_answers(&question_id)
            .await?;

        let mut votes = Vec::with_capacity(answers.len());
        for answer in answers.iter() {
            votes.push(self.answer_repository.count_votes(&answer.id).await?);
        }

        let is_voted: Vec<Option<VoteType>> = match user_id {
            Some(user_id) => {
                let mut is_voted = Vec::with_capacity(answers.len());
                for answer in answers.iter() {
                    is_voted.push(
                        self.answer_repository
                            .find_vote(&answer.id, user_id)
                            .await?
                            .map(|vote| vote.vote_type.into()),
                    );
                }
                is_voted
            }
            None => vec![None; answers.len()],
        };

        let creators = answers.iter().map(|a| a.creator_id).collect::<Vec<_>>();
        let creators = self
            .user_grpc_client
            .lock()
            .await
            .get_users(&creators)
            .await?;

        let mut comments = Vec::with_capacity(answers.len());
        for answer in answers.iter() {
            comments.push(
                self.comment_grpc_client
                    .lock()
                    .await
                    .get_comments(&answer.id, user_id, CommentType::Answer)
                    .await?,
            );
        }

        Ok(AnswersResponse(
            answers
                .into_iter()
                .zip(creators.into_iter())
                .zip(comments.into_iter())
                .zip(is_voted.into_iter())
                .zip(votes.into_iter())
                .map(|((((answer, creator), comments), is_voted), votes)| {
                    let Answer {
                        id,
                        text,
                        created_at,
                        updated_at,
                        sample_id,
                        ..
                    } = answer;
                    AnswerResponse {
                        id,
                        text,
                        created_at,
                        updated_at,
                        creator,
                        sample_id,
                        comments,
                        votes,
                        is_voted,
                    }
                })
                .collect(),
        ))
    }

    pub async fn vote(
        &self,
        vote_create: &AnswerVoteCreate,
        answer_id: &Uuid,
        user_id: &Uuid,
    ) -> Result<String, AnswerError> {
        let _answer = self
            .answer_repository
            .find_by_id(answer_id)
            .await?
            .ok_or_else(|| AnswerError::NotFound(*answer_id))?;

        match self.answer_repository.find_vote(answer_id, user_id).await? {
            Some(mut vote) => {
                let vote_type: AnswerVoteType = (&vote_create.vote_type).into();
                if vote_type == vote.vote_type {
                    self.answer_repository
                        .delete_vote(&vote.answer_id, &vote.user_id)
                        .await?
                } else {
                    vote.vote_type = vote_type;
                    self.answer_repository.update_vote(&vote).await?
                }
            }
            None => {
                self.answer_repository
                    .vote(&AnswerVote {
                        answer_id: *answer_id,
                        user_id: *user_id,
                        vote_type: (&vote_create.vote_type).into(),
                    })
                    .await?
            }
        };

        Ok(self
            .answer_repository
            .count_votes(answer_id)
            .await?
            .to_string())
    }

    pub async fn delete_vote(
        &self,
        answer_id: &Uuid,
        user_id: &Uuid,
    ) -> Result<EmptyResponse, AnswerError> {
        self.answer_repository
            .delete_vote(answer_id, user_id)
            .await?;

        Ok(EmptyResponse::new(StatusCode::NO_CONTENT))
    }

    pub async fn update(
        &mut self,
        data: AnswerUpdate,
        user_id: &Uuid,
    ) -> Result<AnswerResponse, AnswerError> {
        let AnswerUpdate {
            id,
            text,
            sample_id,
        } = data;

        let mut answer = self
            .answer_repository
            .find_by_id(&id)
            .await?
            .ok_or_else(|| AnswerError::NotFound(id))?;

        if answer.creator_id != *user_id {
            return Err(AnswerError::PermissionDenied(
                "User can update only their created answers".into(),
            ));
        }

        answer.text = text;
        answer.sample_id = sample_id;
        answer.updated_at = OffsetDateTime::now_utc();

        self.answer_repository.update(&answer).await?;

        self.find_by_id(&id, &Some(*user_id)).await
    }

    async fn transform_answers(
        &self,
        answers: Vec<Answer>,
        user_id: &Uuid,
    ) -> Result<SimpleAnswersResponse, AnswerError> {
        let mut votes = Vec::with_capacity(answers.len());
        for answer in answers.iter() {
            votes.push(self.answer_repository.count_votes(&answer.id).await?);
        }

        let mut is_voted = Vec::with_capacity(answers.len());
        for answer in answers.iter() {
            is_voted.push(
                self.answer_repository
                    .find_vote(&answer.id, user_id)
                    .await?
                    .map(|vote| vote.vote_type.into()),
            );
        }

        Ok(SimpleAnswersResponse(
            answers
                .into_iter()
                .zip(votes.into_iter())
                .zip(is_voted.into_iter())
                .map(|((answers, votes), is_voted)| {
                    let Answer {
                        id,
                        text,
                        created_at,
                        updated_at,
                        creator_id,
                        sample_id,
                    } = answers;
                    SimpleAnswerResponse {
                        id,
                        text,
                        created_at,
                        updated_at,
                        creator_id,
                        sample_id,
                        votes,
                        is_voted,
                    }
                })
                .collect(),
        ))
    }

    pub async fn find_voted(&self, user_id: &Uuid) -> Result<SimpleAnswersResponse, AnswerError> {
        self.transform_answers(
            self.answer_repository.query_voted_answers(user_id).await?,
            user_id,
        )
        .await
    }

    pub async fn find_created(&self, user_id: &Uuid) -> Result<SimpleAnswersResponse, AnswerError> {
        self.transform_answers(
            self.answer_repository
                .query_created_answers(user_id)
                .await?,
            user_id,
        )
        .await
    }
}
