use std::{hash::Hasher, sync::Arc};

use actix_web::{http::StatusCode, ResponseError};
use async_mutex::Mutex;
use fasthash::{lookup3::Hasher32, FastHasher};
use log::error;
use session_manager::SessionError;
use thiserror::Error;
use time::OffsetDateTime;
use tonic::Status;
use uuid::Uuid;

use crate::{
    api::types::tag::{TagCreate, TagInfoResponse, TagResponse},
    db::{tag::TagRepository, DbError},
    grpc::user::UserGrpcClient,
    model::Tag,
};

#[derive(Error, Debug)]
pub enum TagError {
    #[error("Internal server error: {0:?}")]
    InternalDbError(#[from] DbError),
    #[error("Authentication error: {0}")]
    NotAuthorized(String),
    #[error("Internal server error: {0}")]
    Internal(String),
    #[error("Tag with id {0:?} not found")]
    NotFound(String),
    #[error("Grpc error: {0:?}")]
    GrpcError(String),
}

impl From<SessionError> for TagError {
    fn from(e: SessionError) -> Self {
        match e {
            SessionError::SessionToken(error) => TagError::NotAuthorized(error),
            SessionError::Crypto(error) => TagError::Internal(error),
            SessionError::RedisConnection(error) => TagError::Internal(format!("{}", error)),
        }
    }
}

impl From<Status> for TagError {
    fn from(err: Status) -> Self {
        error!(
            "Grpc error: {}. Metadata: {:?}",
            err.message(),
            err.metadata()
        );
        Self::GrpcError(format!("{:?}", err.code()))
    }
}

impl ResponseError for TagError {
    fn status_code(&self) -> StatusCode {
        match self {
            TagError::NotAuthorized(_) => StatusCode::UNAUTHORIZED,
            TagError::NotFound(_) => StatusCode::NOT_FOUND,
            e => {
                log::error!("{:?}", e);
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    }
}

pub struct TagService {
    tag_repository: TagRepository,
    user_grpc_client: Arc<Mutex<UserGrpcClient>>,
}

impl TagService {
    pub fn new(
        tag_repository: TagRepository,
        user_grpc_client: Arc<Mutex<UserGrpcClient>>,
    ) -> Self {
        Self {
            tag_repository,
            user_grpc_client,
        }
    }

    pub async fn create(&self, data: &TagCreate, user_id: &Uuid) -> Result<TagResponse, TagError> {
        let TagCreate { name } = data.clone();
        let pk = Uuid::new_v4();

        let mut hasher = Hasher32::new();
        hasher.write(name.as_bytes());

        let tag = Tag {
            pk,
            id: hasher.finish().to_string(),
            name,
            created_at: OffsetDateTime::now_utc(),
            creator_id: *user_id,
        };

        self.tag_repository.save(&tag).await?;

        Ok(tag.into())
    }

    pub async fn find_by_id(&self, id: &str) -> Result<TagResponse, TagError> {
        self.tag_repository
            .find_by_id(id)
            .await?
            .map(|t| t.into())
            .ok_or_else(|| TagError::NotFound(id.into()))
    }

    pub async fn get_by_id(&self, id: &str) -> Result<Tag, TagError> {
        self.tag_repository
            .find_by_id(id)
            .await?
            .ok_or_else(|| TagError::NotFound(id.into()))
    }

    pub async fn get_tag_info(&self, tag_id: &str) -> Result<TagInfoResponse, TagError> {
        let Tag {
            pk,
            id,
            created_at,
            name,
            creator_id,
        } = self
            .tag_repository
            .find_by_id(tag_id)
            .await?
            .ok_or_else(|| TagError::NotFound(tag_id.into()))?;

        let users = self
            .user_grpc_client
            .lock()
            .await
            .get_users(&[creator_id])
            .await?;
        let creator = users.get(0).unwrap();

        Ok(TagInfoResponse {
            id,
            name,
            created_at,
            creator: creator.clone(),
            questions_amount: self.tag_repository.count_tag_questions(&pk).await?,
        })
    }

    pub async fn add_tag_to_question(
        &self,
        question_id: &Uuid,
        tag_id: &str,
    ) -> Result<Option<()>, TagError> {
        let tag_id = match self.tag_repository.find_by_id(tag_id).await? {
            Some(tag) => tag.pk,
            None => return Ok(None),
        };

        Ok(Some(
            self.tag_repository
                .add_tag_to_question(question_id, &tag_id)
                .await?,
        ))
    }

    pub async fn get_all_tags(&self) -> Result<Vec<TagResponse>, TagError> {
        Ok(self
            .tag_repository
            .get_all_tags()
            .await?
            .into_iter()
            .map(|t| t.into())
            .collect())
    }

    pub async fn question_tags(&self, question_id: &Uuid) -> Result<Vec<TagResponse>, TagError> {
        Ok(self
            .tag_repository
            .get_question_tags(question_id)
            .await?
            .into_iter()
            .map(|t| t.into())
            .collect())
    }

    pub async fn remove_question_tags(&self, question_id: &Uuid) -> Result<(), TagError> {
        self.tag_repository.remove_tags(question_id).await?;

        Ok(())
    }
}
