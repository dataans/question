FROM eu.gcr.io/dataans/question_builder:5 as builder

COPY ./Cargo.toml ./Cargo.toml
RUN rm src/*.rs

ADD src ./src

RUN rm ./target/release/deps/question*
RUN cargo build --release

FROM frolvlad/alpine-glibc:glibc-2.29
ARG APP=/usr/src/app

EXPOSE 8000

COPY --from=builder /question/target/release/question ./question

CMD ["./question"]
